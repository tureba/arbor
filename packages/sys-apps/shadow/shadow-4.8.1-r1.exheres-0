# Copyright 2008-2012 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'shadow-4.1.2-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require github [ user=shadow-maint release=${PV} suffix=tar.xz ] \
    pam \
    option-renames [ renames=[ 'attr xattr' ] ]
require systemd-service

SUMMARY="User and group management related utilities"

LICENCES="|| ( BSD-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    acl   [[ description = [ Add support for ACL when copying user directories ] ]]
    btrfs [[ description = [ Add support for btrfs subvolumes for user homes ] ]]
    caps
    sssd  [[ description = [ Add support for flushing sssd caches ] ]]
    xattr [[ description = [ Add support for extended attributes when copying user directories ] ]]

    ( libc: musl )
    ( linguas: bs ca cs da de dz el es eu fi fr gl he hu id it ja kk km ko nb ne nl nn pl pt pt_BR
               ro ru sk sq sv tl tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        acl? ( sys-apps/acl )
        btrfs? ( sys-fs/btrfs-progs )
        caps? ( sys-libs/libcap )
        !libc:musl? ( dev-libs/libxcrypt:= )
        sssd? ( sys-auth/sssd )
        xattr? ( sys-apps/attr )
        sys-libs/cracklib[>=2.8.3]
        sys-libs/pam[>=1.0.1]
        !sys-apps/util-linux[<2.24] [[
            description = [ sys-apps/util-linux 2.24 installs nologin previously provided by this package ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --bindir="/usr/$(exhost --target)/bin"
    --enable-nls
    --enable-shadowgrp
    --enable-static
    --disable-shared
    --disable-vendordir
    --with-group-name-max-length=1024
    --with-libcrack
    --with-libpam
    --with-nscd
    --with-sha-crypt
    --with-su
    --without-audit
    --without-bcrypt
    --without-selinux
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    acl
    btrfs
    'caps fcaps'
    sssd
    'xattr attr'
)

DEFAULT_SRC_INSTALL_PARAMS=( suidperms=4711 ubindir='${bindir}' usbindir='${sbindir}' )
DEFAULT_SRC_INSTALL_EXTRA_PREFIXES="doc/"
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( HOWTO WISHLIST console.c.spec.txt )

src_prepare() {
    default

    edo sed -i \
        -e '/^LOGIN_RETRIES/   s:5$:3:'   \
        -e '/^#MD5_CRYPT_ENAB/ s:no:yes:' \
        -e '/^#MD5_CRYPT_ENAB/ s:^#::'    \
        etc/login.defs
}

src_install() {
    default

    # Remove libshadow and libmisc; see Gentoo bug 37725 and the following
    # comment from shadow's README.linux:
    #   Currently, libshadow.a is for internal use only, so if you see
    #   -lshadow in a Makefile of some other package, it is safe to
    #   remove it.
    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib{misc,shadow}.{a,la}

    install_systemd_files
    insinto /usr/share/factory/etc
    insopts -m0600
    doins "${FILES}"/securetty

    # needed for 'adduser -D'
    insinto /usr/share/factory/etc/default
    insopts -m0600 ; doins "${FILES}"/useradd

    insinto /usr/share/factory/etc
    insopts -m0644
    newins etc/login.defs login.defs

    dopamd "${FILES}"/pam.d-include/{su,passwd,shadow}
    newpamd "${FILES}"/login.pamd.2 login

    for x in passwd chpasswd chgpasswd; do
        newpamd "${FILES}"/pam.d-include/passwd ${x}
    done

    for x in chage chsh chfn newusers user{add,del,mod} group{add,del,mod} ; do
        newpamd "${FILES}"/pam.d-include/shadow ${x}
    done

    # comment out login.defs options that pam hates
    edo sed -i -f "${FILES}"/login_defs_pam.sed "${IMAGE}"/usr/share/factory/etc/login.defs

    # remove manpages that pam will install for us
    # and/or don't apply when using pam
    edo find "${IMAGE}"/usr/share/man '(' -name 'limits.5*' -o -name 'suauth.5*' ')' -delete

    # Remove manpages that are handled by other packages
    edo find "${IMAGE}"/usr/share/man '(' -name id.1 -o -name passwd.5 -o -name getspnam.3 -o -name nologin.8 ')' -delete

    # Remove nologin which is installed by util-linux[>=2.24]
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/nologin

    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    # Enable shadow groups (we need ROOT=/ here, as grpconv only operates on / ...).
    if [[ ${ROOT} == / && ! -f /etc/gshadow ]] ; then
        if grpck -r ; then
            grpconv
        else
            eerror "Running 'grpck -r' returned errors. Please run it by hand, and then"
            eerror "run 'grpconv' afterwards."
        fi
    fi

    if option caps; then
        setcap cap_setuid+ep /usr/$(exhost --target)/bin/newuidmap
        setcap cap_setgid+ep /usr/$(exhost --target)/bin/newgidmap
    fi
}

