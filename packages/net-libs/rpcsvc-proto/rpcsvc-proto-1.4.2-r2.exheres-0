# Copyright 2018-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=thkukuk release=v${PV} suffix=tar.xz ]

SUMMARY="rpcsvc protocol definitions from glibc"
DESCRIPTION="
This package contains rpcsvc proto.x files from glibc, which are missing in libtirpc. Additional it
contains rpcgen, which is needed to create header files and sources from protocol files. This
package is only needed, if glibc is installed without the deprecated sunrpc functionality and
libtirpc should replace it.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
    build+run:
        !sys-libs/glibc[<2.31-r3] [[
            description = [ Part of glibc when built with the deprecated sunrpc functionality ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
)

src_prepare() {
    # fix hardcoded cpp
    edo sed \
        -e "s:/lib/cpp:/usr/$(exhost --target)/bin/$(exhost --tool-prefix)cpp:" \
        -e 's:\(CPP = "\)cpp:\1'$(exhost --tool-prefix)cpp: \
        -i rpcgen/rpc_main.c

    default
}

