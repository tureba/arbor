# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License

require bash-completion gnome.org [ suffix=tar.xz ]
require meson
require gtk-icon-cache test-dbus-daemon
require gsettings
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require utf8-locale
require systemd-service

export_exlib_phases src_test src_install

SUMMARY="A low-level configuration system"
HOMEPAGE="https://wiki.gnome.org/action/show/Projects/${PN}"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gtk-doc
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
    suggestion:
        gnome-desktop/dconf-editor [[ description = [ graphical tool to manage dconf ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbash_completion=false
    -Dman=true
    -Dsystemduserunitdir=${SYSTEMDUSERUNITDIR}
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    vapi
)

dconf_src_test() {
    unset DISPLAY
    require_utf8_locale
    test-dbus-daemon_run-tests meson_src_test
}

dconf_src_install() {
    # gsettings_src_install uses `default`
    export GSETTINGS_DISABLE_SCHEMAS_COMPILE=1
    meson_src_install
    unset GSETTINGS_DISABLE_SCHEMAS_COMPILE

    dobashcompletion "${MESON_SOURCE}/bin/completion/dconf"
}

