# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ "slapd.service" ] ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ none ] ]

# src_test_expensive
export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="An open source implementation of the Lightweight Directory Access Protocol"
DESCRIPTION="
The suite includes:
* slapd - stand-alone LDAP daemon (server)
* slurpd - stand-alone LDAP update replication daemon
* libraries implementing the LDAP protocol
* utilities, tools, and sample clients
"
HOMEPAGE="https://www.openldap.org"
DOWNLOADS="${HOMEPAGE}/software/download/OpenLDAP/openldap-release/${PNV}.tgz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/software/release/changes.html"
UPSTREAM_DOCUMENTATION="
${HOMEPAGE}/doc/admin/          [[ lang = en description = [ Administration guide ] ]]
${HOMEPAGE}/faq/                [[ lang = en description = [ FAQ ] ]]
${HOMEPAGE}/software/man.cgi    [[ lang = en description = [ Manual pages ] ]]
"

LICENCES="OpenLDAP-2.8"
SLOT="0"
MYOPTIONS="
    debug
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Expensive tests
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.29]
    build+run:
        user/ldap
        group/ldap
        dev-db/lmdb[>=0.9.17]
        dev-libs/gmp:=
        net-libs/cyrus-sasl[>=2.1.21]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        virtual/syslog
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --libexecdir=/usr/$(exhost --target)/libexec/openldap
    --localstatedir=/var
    --disable-static

    --enable-dynamic
    --enable-ipv6
    --enable-shared
    --enable-syslog
    --enable-versioning
    --with-cyrus-sasl
    --with-threads
    --with-tls=openssl # openssl/gnutls
    --with-mp=gmp

    # Whether select yields when using pthreads
    # Cannot be tested with cross; is always yes
    # for both glibc and musl libc.
    --with-yielding_select=yes

    --enable-slapd
    --enable-spasswd

    # Default-on backends
    --enable-mdb
    --enable-relay

    # No dependencies
    --enable-dnssrv # Referrals via DNS SRV
    --enable-ldap   # single-server proxying
    --enable-meta   # proxying of multiple servers
    --enable-asyncmeta # asynchronous proxying of multiple servers
    --enable-null   # Succeeds at nothing, every time
    --enable-sock   # Allows external backends over a socket

    # Default-disabled backends
    --disable-passwd # No dependencies, but meant for demonstration only
    --disable-perl   # Custom backends in Perl
    --disable-sql    # needs iodbc, unixodbc, or odbc32
    --disable-wt     # WiredTiger backend
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    systemd
)

# Build fails sporadically when jobs > 1
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_PARAMS=( STRIP="" )

AT_M4DIR=( build )

openldap_src_prepare() {
    default

    edo sed -e '/^#define LDAPI_SOCK/s/ldapi/openldap" LDAP_DIRSEP "slapd.sock/' -i ./include/ldap_defaults.h
    edo sed -re '/^(pidfile|argsfile)/s:run/:run/openldap/:' -i ./servers/slapd/slapd.conf

    # Shared objects should have a+rx,u+w permissions
    edo sed -e '/LTINSTALL/s/-m 644/-m 755/' -i ./libraries/*/Makefile.in

    # remove the bundled lmdb
    edo rm -rf libraries/liblmdb

    # do not strip
    edo sed \
        -e '/STRIP_OPTS/d' \
        -i build/top.mk

    eautoconf
}

openldap_src_configure() {
    default

# Work around OpenLDAP's crap cross-compilation support
cat >> include/portable.h <<'OVERRIDES'
#define URANDOM_DEVICE "/dev/urandom"
#undef NEED_MEMCMP_REPLACEMENT
OVERRIDES
}

openldap_src_compile() {
    emake depend
    default
}

# tests want to access /dev/log
#openldap_src_test_expensive() {
#    emake test
#}

openldap_src_install() {
    default

    edo rmdir "${IMAGE}"/var/run

    install_systemd_files
    insinto /usr/share/factory/etc/conf.d
    doins "${FILES}"/slapd.conf
}

