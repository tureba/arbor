# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'graphviz-2.18.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require gitlab [ new_download_scheme=true ]
require python [ blacklist=2 with_opt=true multibuild=false ]
require autotools [ supported_autoconf=[ 2.7 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Open Source Graph Visualization Software"
HOMEPAGE+=" https://www.${PN}.org"

LICENCES="EPL-1.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    gtk [[ description = [ Provides canvas support and cairo renderer ] ]]
    pdf
    postscript [[ description = [ Support for PostScript utilizing Ghostscript ] ]]
    python
    svg [[ description = [ Support node shapes in svg output ] ]]
    webp [[ description = [ Support for the Webp image format ] ]]
    X
"

DEPENDENCIES="
    build:
        sys-devel/flex
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        python? ( dev-lang/swig[python] )
    build+run:
        dev-libs/expat[>=2.0.0] [[ note = [ Required for HTML-like labels ] ]]
        dev-libs/glib:2[>=2.11]
        media-libs/fontconfig[>=2.3.95]
        media-libs/freetype:2[>=2.1.10]
        media-libs/libpng:=[>=1.2]
        x11-libs/cairo[>=1.1.10]
        x11-libs/pango[>=1.12.4]
        gtk? ( x11-libs/gtk+:2[>=2.7] )
        pdf? ( app-text/poppler[glib] )
        postscript? ( app-text/ghostscript )
        svg? ( gnome-desktop/librsvg:2[>=2.36.0] )
        webp? ( media-libs/libwebp:= )
        X? ( x11-libs/libXaw )
"

#TODO_DEPENDENCIES+="
#    build:
#        lua? ( dev-lang/swig[lua] )
#        perl? ( dev-lang/swig[perl] )
#        ruby? ( dev-lang/swig[ruby] )
#    build+run:
#        lua? ( dev-lang/lua:= )
#        perl? ( dev-lang/perl:= )
#        ruby? ( dev-lang/ruby:= )
#"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --htmldir=/usr/share/doc/${PN}/${SLOT}/html
    --pdfdir=/usr/share/doc/${PN}/${SLOT}/pdf
    --enable-ltdl
    --enable-nls
    --disable-debug
    --disable-go
    --disable-ltdl-install
    --disable-python
    --disable-python2
    --disable-static
    --with-fontconfig
    --with-freetype2
    --with-jpeg
    --with-pangocairo
    --with-png
    --without-devil
    --without-libgd
    --without-visio
    # Deprecated crap
    --without-gdk
    --without-gdk-pixbuf
    --without-ming
    --without-gtkgl
    # Need to fix install path for these bindings
    --disable-{perl,ruby}
    # dev-lang/lua doesn't install a .so, -fPIC errors
    --disable-lua
    # No swig-bindings yet
    --disable-{guile,java,io,ocaml,php,r,sharp,tcl}
    # Requires extra packages (gts, smyrna, etc.), and doesn't seem to work yet (cgraph)
    --without-{ann,cgraph,glade,gtkglext,gts,lasi,smyrna}
    # disable gvedit, requires Qt
    --without-qt
)

#TODO_DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
#    perl ruby
#)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'python python3'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'gtk'
    'pdf poppler'
    'postscript ghostscript'
    'svg rsvg'
    'webp'
    'X x'
)

src_prepare() {
    # respect selected PYTHON_ABI
    export PYTHON3=${PYTHON}

    # Copy from autogen.sh
    edo cat > version.m4 << END

m4_define([graphviz_version_major],[$(ever major)])
m4_define([graphviz_version_minor],[$(ever range 2)])
m4_define([graphviz_version_micro],[$(ever range 3)])
m4_define([graphviz_collection],[stable])

m4_define([graphviz_version_date],[0])
m4_define([graphviz_change_date],[])
m4_define([graphviz_git_date],[0])
m4_define([graphviz_author_name],[])
m4_define([graphviz_author_email],[])
m4_define([graphviz_version_commit],[])
END

    autotools_src_prepare
}

src_install() {
    default

    edo mv "${IMAGE}"/usr/share/{${PN}/*,doc/${PN}/${SLOT}/}
    edo mv "${IMAGE}"/usr/share/doc/${PN}/${SLOT}/{doc/*,}
    # Disabled language bindings leave stray directories, as do other things..
    [[ -d "${IMAGE}"/usr/share/doc/${PN}/${SLOT}/demo/{pathplan_data,} ]] && edo rmdir "${IMAGE}"/usr/share/doc/${PN}/${SLOT}/demo/{pathplan_data,}
    edo find "${IMAGE}"/usr/ -type d -empty -delete
}

pkg_postinst() {
    dot -c
}

