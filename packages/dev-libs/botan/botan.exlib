# Copyright 2009-2012 Pierre Lejeune <superheron@gmail.com>
# Copyright 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_install

MY_PNV="Botan-${PV}"

SUMMARY="Botan, a C++ crypto library"
DESCRIPTION="
Botan is a BSD-licensed crypto library written in C++. In provides applications with the ability to
use a number of cryptographic algorithms, as well as X.509 certificates and CRLs, PKCS #10
certificate requests, a filter/pipe message processing system, and a wide variety of other
features, all written in portable C++.
"
HOMEPAGE="https://botan.randombit.net/"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doxygen/"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news.html"

LICENCES="BSD-2"
MYOPTIONS="
    boost [[ description = [ Builds Boost.Python wrapper ] ]]
    doc
    sqlite
    ( amd64_cpu_features: ssse3 sse4.1 sse4.2 avx2 )
    ( x86_cpu_features: sse2 ssse3 sse4.1 sse4.2 avx2 )
    ( platform: amd64 x86 )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        doc? ( dev-python/Sphinx )
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib
        boost? ( dev-libs/boost )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        sqlite? ( dev-db/sqlite:3 )
    run:
        !dev-libs/botan:2[<2.14.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_COMPILE_PARAMS=(
    AR="${AR}"
    CXX=${CXX}
    LIB_OPT="${CXXFLAGS} -finline-functions"
    RANLIB=${RANLIB}
)
DEFAULT_SRC_INSTALL_PARAMS=( DOCDIR="${IMAGE}"/usr/share/doc/${PN}/${SLOT} )
DEFAULT_SRC_TEST_PARAMS=( CXX=${CXX} )
DEFAULT_SRC_INSTALL_EXCLUDE=( readme.txt )

botan_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}                ${PN}-${SLOT}
        /usr/${host}/include/${PN}-2          ${PN}-2-${SLOT}
        /usr/${host}/lib/lib${PN}-2.so        lib${PN}-2-${SLOT}.so
        /usr/${host}/lib/pkgconfig/botan-2.pc ${PN}-2-${SLOT}.pc
        /usr/${host}/lib/python$(python_get_abi)/site-packages/${PN}2.py ${PN}2-${SLOT}.py
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

