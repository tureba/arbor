# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libpcap-0.9.8.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A library for network packet capturing"
HOMEPAGE="https://www.tcpdump.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    bluetooth
    dbus
    nl80211 [[ description = [ Support monitor mode on nl80211 devices ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex[>=2.5.31]
        dbus? ( virtual/pkg-config )
    build+run:
        bluetooth? ( net-wireless/bluez[>=5.32] )
        dbus? ( sys-apps/dbus[>=1.0.0] )
        nl80211? ( net-libs/libnl:3.0 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.10.0-usbmon.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-usb
    --disable-netmap
    --disable-optimizer-dbg
    --disable-rdma
    --disable-remote
    --with-pcap=linux
    --without-dag
    --without-dpdk
    --without-septel
    --without-snf
    --without-turbocap
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( bluetooth dbus )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'nl80211 libnl' )
DEFAULT_SRC_INSTALL_EXCLUDE=( README.{Win32,aix,dag,hpux,macosx,septel,tru64} )

src_prepare() {
    # find prefixed pkg-config
    # TODO: fix upstream
    edo sed \
        -e "s/^\(AC_CHECK_PROG(.*\)pkg-config/\1${PKG_CONFIG}/" \
        -i configure.ac

    autotools_src_prepare
}

