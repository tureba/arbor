# Copyright 2020 Tom Briden <tom@decompile.me.uk>
# Copyright 2020 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user="Yubico" tag="${PV}" ]
require cmake
require flag-o-matic
require udev-rules

SUMMARY="Provides library functionality for FIDO 2.0, including communication with a device over USB"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    doc
    libc: glibc musl
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        doc? ( sys-apps/mandoc )
    build+run:
        dev-libs/libcbor
        sys-libs/zlib
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        providers:systemd? ( sys-apps/systemd )
        !dev-libs/libu2f-host[<1.1.10-r1] [[
            description = [ libfido2 now provides the udev rules ]
            resolution = uninstall-blocked-after
        ]]
"

src_configure() {
    local cmake_params=(
        $(cmake_build doc MANPAGES)
        $(option doc -DMANDOC_PATH:PATH=/usr/$(exhost --target)/bin/mandoc)
        # examples build fails (last checked 1.6.0)
        -DBUILD_EXAMPLES:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_STATIC_LIBS:BOOL=FALSE
        -DBUILD_TOOLS:BOOL=TRUE
        -DFUZZ:BOOL=OFF
        -DLIBFUZZER:BOOL=OFF
        -DUSE_HIDAPI:BOOL=OFF
        -DUSE_WINHELLO:BOOL=OFF
        -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
        -DUDEV_RULES_DIR:PATH=${UDEVRULESDIR}
    )

    if option libc:musl; then
        append-flags '-Wno-overflow' # ioctl takes int on musl
    fi

    ecmake "${cmake_params[@]}"
}

