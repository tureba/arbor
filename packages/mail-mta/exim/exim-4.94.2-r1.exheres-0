# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon exim-4.69-r1.ebuild from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require alternatives

SUMMARY="A highly configurable, drop-in replacement for sendmail"
HOMEPAGE="https://www.exim.org"
DOWNLOADS="
    https://ftp.exim.org/pub/${PN}/${PN}${PV%%.*}/${PNV}.tar.bz2
    doc? ( https://ftp.exim.org/pub/${PN}/${PN}${PV%%.*}/${PN}-html-${PV}.tar.bz2 )
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
# XXX X? for eximon
MYOPTIONS="
    dnsdb         [[ description = [ Adds support for a DNS search for a record whose domain name is the supplied query ] ]]
    doc
    dovecot-sasl  [[ description = [ Adds support for Dovecot's authentication ] ]]
    embedded-perl [[ description = [ Use Perl code in Exim's string manipulation language. Using embedded Perl costs quite a lot of resources. Enable only if needed. ] ]]
    exiscan       [[ description = [ Adds support for content filtering ] ]]
    lmtp          [[ description = [ Adds support for lmtp ] ]]
    mbx           [[ description = [ Adds support for UW's mbx format ] ]]
    pam
    sasl
    tcpd

    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/pcre
        sys-libs/db:5.3[>=4&<6]
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        pam? ( sys-libs/pam )
        embedded-perl? ( dev-lang/perl:= )
        sasl? ( net-libs/cyrus-sasl )
        tcpd? ( sys-apps/tcp-wrappers )
        group/mail
        user/mail
        user/postmaster
"
# TODO Eximon
EXIMON="
    build:
        X? ( x11-proto/xorgproto )
    build+run:
        X? (
            x11-libs/libX11
            x11-libs/libXaw
            x11-libs/libXmu
            x11-libs/libXt
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-4.94-maildir.patch
    "${FILES}"/${PN}-4.94-exherbo-defaults.patch
)

DEFAULT_SRC_COMPILE_PARAMS=( build=Exim-Exherbo )

src_prepare() {
    default

    # TODO: fix upstream
    edo sed \
        -e "s:pkg-config:$(exhost --tool-prefix)pkg-config:g" \
        -i scripts/Configure-Makefile

    edo cp "${WORK}"/src/EDITME Local/Makefile
    # XXX option X && edo cp ../exim_monitor/EDITME eximon.conf

    edo sed -e "s:\(\# \)\?AR=ar:AR=${AR}:" \
        -e "/^BIN_DIRECTORY/ s:/usr/bin:/usr/$(exhost --target)/bin:" \
        -e "/MAKE_SHELL/ s:^.*$:MAKE_SHELL=sh:" \
        -e "/_COMMAND/ s:^\(\# \)\?\([[:upper:]]\+_COMMAND=\)\(/usr\)\?/bin/:\2:p" \
        -i Local/Makefile

    edo cat <<EOF >> Local/Makefile
CC = ${CC}
RANLIB = ${RANLIB}

CFLAGS += ${CFLAGS} -I/usr/$(exhost --target)/include/db5.3
LFLAGS += ${LDFLAGS} -ldb-5.3
EOF

    edo sed -e 's/^buildname=.*/buildname=Exim-Exherbo/g' -i Makefile

    uncomment_if_option_enabled() {
        option ${1} && edo sed -e "/${2}/s/^#[[:space:]]*//" -i Local/Makefile
    }

    uncomment_if_option_enabled dnsdb   LOOKUP_DNSDB=yes
    uncomment_if_option_enabled dovecot-sasl AUTH_DOVECOT=yes
    uncomment_if_option_enabled lmtp    TRANSPORT_LMTP=yes
    uncomment_if_option_enabled mbx     SUPPORT_MBX=yes
    uncomment_if_option_enabled pam     SUPPORT_PAM=yes
    uncomment_if_option_enabled embedded-perl EXIM_PERL=perl.o
    uncomment_if_option_enabled sasl    AUTH_CYRUS_SASL=yes
    # XXX Verify whether the path used in "${FILES}"/${PNV}-exherbo-defaults.patch
    uncomment_if_option_enabled sasl    CYRUS_SASLAUTHD_SOCKET=
    uncomment_if_option_enabled tcpd    USE_TCP_WRAPPERS=yes
    uncomment_if_option_enabled exiscan WITH_CONTENT_SCAN=yes
    # XXX uncomment_if_option_enabled X       EXIM_MONITOR=eximon.bin

cat <<EOF >> Local/Makefile
# Enable ipv6
HAVE_IPV6=YES
IPV6_USE_INET_PTON=yes
EOF

    local extralibs=
    option pam && extralibs+="-lpam "
    option sasl && extralibs+="-lsasl2 "
    option tcpd && extralibs+="-lwrap "

    if [[ -n ${extralibs} ]]; then
cat <<EOF >> Local/Makefile
# Support for pam, sasl, tcpd, ..
EXTRALIBS=${extralibs}
EOF
    fi

    # TODO ldap mysql postgresql sqlite redis idn

    # TODO When there's a properly slotted sys-libs/db:
    #   INCLUDE=-I/usr/include/db1
    #   DBMLIB=-ldb1
}

src_install() {
    # For the mail aliases file, the buildsystem doesn't create this
    dodir /etc/mail

    # scripts/exim_install runs the exim binary we just build.
    # When exim runs, it recreates the spooldir, if it doesn't exist, which causes access violations.
    # There doesn't seem to be a clean way to not run exim, or to make exim not
    # do that, so addfilter will have to do..
    esandbox addfilter /var/spool/exim
    default
    esandbox rmfilter /var/spool/exim

    alternatives_for mta ${PN} 10 /usr/$(exhost --target)/bin/sendmail ${PN}
    edo mv "${IMAGE}"/etc/mail/{,${PN}.}aliases
    alternatives_for mta ${PN} 10 /etc/mail/aliases ${PN}.aliases

    dodoc "${WORK}"/doc/!(cve-*)
    doman "${WORK}"/doc/${PN}.8

    if option doc; then
        insinto /usr/share/doc/${PN}/${SLOT}/
        doins -r "${WORKBASE}"/${PN}-html-${PV}/doc/html
    fi

    diropts "-m0750"
    dodir /var/log/${PN} /var/spool/${PN}
    keepdir /var/log/${PN} /var/spool/${PN}
    edo chown -R mail:mail "${IMAGE}"/var/{log,spool}/${PN}
}

