Title: Postgresql < 9.6.9 and < 10.4 need post installation steps
Author: Arnaud Lefebvre <a.lefebvre@outlook.fr>
Content-Type: text/plain
Posted: 2018-05-21
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-db/postgresql

Postgresql < 9.6.9 and < 10.4 need post installation steps to fix CVE-2018-1115
in the adminpack extension and to also fix the "incorrect volatility and
parallel-safety markings" issue.

Those steps can be found at the end of the postgresql's news
article here: https://www.postgresql.org/about/news/1851/
